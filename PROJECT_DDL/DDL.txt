CREATE SCHEMA IF NOT EXISTS `EPL_DATA` DEFAULT CHARACTER SET utf8 ;

-- Temporary view structure for view `avg_odds_bet_sites`

DROP TABLE IF EXISTS `avg_odds_bet_sites`;
/*!50001 DROP VIEW IF EXISTS `avg_odds_bet_sites`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `avg_odds_bet_sites` AS SELECT 
 1 AS `bet_sites_name`,
 1 AS `avg_home_odds`*/;
SET character_set_client = @saved_cs_client;

-- Table structure for table `bet_sites`
--

DROP TABLE IF EXISTS `bet_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bet_sites` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bet_sites_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


-- Table structure for table `bet_sites_odds`
--

DROP TABLE IF EXISTS `bet_sites_odds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bet_sites_odds` (
  `home_odds` float NOT NULL,
  `away_odds` float NOT NULL,
  `draw_odds` float NOT NULL,
  `match_id` int NOT NULL,
  `bet_sites_id` int NOT NULL,
  PRIMARY KEY (`match_id`,`bet_sites_id`),
  KEY `fk_Bet_sites_odds_Bet_sites1` (`bet_sites_id`),
  KEY `fk_Bet_sites_odds_Matches1_idx` (`match_id`),
  CONSTRAINT `fk_Bet_sites_odds_Bet_sites1` FOREIGN KEY (`bet_sites_id`) REFERENCES `bet_sites` (`id`),
  CONSTRAINT `fk_Bet_sites_odds_Matches1` FOREIGN KEY (`match_id`) REFERENCES `matches` (`match_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

-- Table structure for table `denormalized`
--

DROP TABLE IF EXISTS `denormalized`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `denormalized` (
  `match_id` int NOT NULL,
  `match_date` date NOT NULL,
  `home_team` varchar(45) DEFAULT NULL,
  `away_team` varchar(45) DEFAULT NULL,
  `home_team_goal` int DEFAULT NULL,
  `away_team_goal` int DEFAULT NULL,
  `result` varchar(1) DEFAULT NULL,
  `ht_home_team_goal` int DEFAULT NULL,
  `ht_away_goal` int DEFAULT NULL,
  `ht_result` varchar(1) DEFAULT NULL,
  `referee_id` int DEFAULT NULL,
  `ref_name` varchar(45) DEFAULT NULL,
  `home_shoots` int DEFAULT NULL,
  `away_shoot` int DEFAULT NULL,
  `hometarget` int DEFAULT NULL,
  `awaytarget` int DEFAULT NULL,
  `homefouls` int DEFAULT NULL,
  `awayfouls` int DEFAULT NULL,
  `homecorner` int DEFAULT NULL,
  `awaycorner` int DEFAULT NULL,
  `homeyellow` int DEFAULT NULL,
  `awayyellow` int DEFAULT NULL,
  `homered` int DEFAULT NULL,
  `awayred` int DEFAULT NULL,
  `bet365homeodd` float DEFAULT NULL,
  `bet365drawodd` float DEFAULT NULL,
  `bet365awayodd` float DEFAULT NULL,
  `betwinhomeodd` float DEFAULT NULL,
  `betwindrawodd` float DEFAULT NULL,
  `betwinawayodd` float DEFAULT NULL,
  `interwettenhomeodd` float DEFAULT NULL,
  `interwttendrawodd` float DEFAULT NULL,
  `interwettenawayodd` float DEFAULT NULL,
  `laybrokeshomeodd` float DEFAULT NULL,
  `latbrokesdrawodd` float DEFAULT NULL,
  `latbrokesawayodd` float DEFAULT NULL,
  `pinnaclehomeodd` float DEFAULT NULL,
  `pinnacledrawodd` float DEFAULT NULL,
  `pinnacleawayodd` float DEFAULT NULL,
  `williamhomeodd` float DEFAULT NULL,
  `williamdrawodd` float DEFAULT NULL,
  `williamawayodd` float DEFAULT NULL,
  `vchomeodd` float DEFAULT NULL,
  `vcdrawodd` float DEFAULT NULL,
  `vcawayod` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

-- Table structure for table `goals_results`
--

DROP TABLE IF EXISTS `goals_results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `goals_results` (
  `ht_home_goal` int NOT NULL,
  `ht_away_goal` int NOT NULL,
  `ft_home_goal` int NOT NULL,
  `ft_away_goal` int NOT NULL,
  `ht_result` varchar(1) NOT NULL,
  `ft_result` varchar(1) NOT NULL,
  `match_id` int NOT NULL,
  PRIMARY KEY (`match_id`),
  CONSTRAINT `fk_Goals_Results_Matches1` FOREIGN KEY (`match_id`) REFERENCES `matches` (`match_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

-- Temporary view structure for view `managed_referee`
--

DROP TABLE IF EXISTS `managed_referee`;
/*!50001 DROP VIEW IF EXISTS `managed_referee`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `managed_referee` AS SELECT 
 1 AS `referee_name`,
 1 AS `count(r.referee_id)`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `matches`
--

DROP TABLE IF EXISTS `matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `matches` (
  `match_id` int NOT NULL AUTO_INCREMENT,
  `home_team` varchar(45) NOT NULL,
  `away_team` varchar(45) NOT NULL,
  `match_date` date NOT NULL,
  PRIMARY KEY (`match_id`)
) ENGINE=InnoDB AUTO_INCREMENT=381 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

-- Table structure for table `matches_referees`
--

DROP TABLE IF EXISTS `matches_referees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `matches_referees` (
  `referee_id` int NOT NULL,
  `match_id` int NOT NULL,
  PRIMARY KEY (`referee_id`,`match_id`),
  KEY `fk_matches_referees_Matches1_idx` (`match_id`),
  CONSTRAINT `fk_matches_referees_Matches1` FOREIGN KEY (`match_id`) REFERENCES `matches` (`match_id`),
  CONSTRAINT `fk_matches_referees_Referees` FOREIGN KEY (`referee_id`) REFERENCES `referees` (`referee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

-- Table structure for table `referee_act`
--

DROP TABLE IF EXISTS `referee_act`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `referee_act` (
  `home_red` int NOT NULL,
  `away_red` int NOT NULL,
  `home_yellow` int NOT NULL,
  `away_yellow` int NOT NULL,
  `match_id` int NOT NULL,
  `referee_id` int NOT NULL,
  `home_fouls` int NOT NULL,
  `away_fouls` int NOT NULL,
  `home_corners` int NOT NULL,
  `away_corners` int NOT NULL,
  PRIMARY KEY (`match_id`),
  KEY `fk_Referee_act_Referees1_idx` (`referee_id`),
  CONSTRAINT `fk_Referee_act_Matches1` FOREIGN KEY (`match_id`) REFERENCES `matches` (`match_id`),
  CONSTRAINT `fk_Referee_act_Referees1` FOREIGN KEY (`referee_id`) REFERENCES `referees` (`referee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

-- Table structure for table `referees`
--

DROP TABLE IF EXISTS `referees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `referees` (
  `referee_id` int NOT NULL AUTO_INCREMENT,
  `referee_name` varchar(45) NOT NULL,
  PRIMARY KEY (`referee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shoots`
--

DROP TABLE IF EXISTS `shoots`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shoots` (
  `home_shoot` int NOT NULL,
  `away_shoot` int NOT NULL,
  `home_shoot_target` int NOT NULL,
  `away_shoot_target` int NOT NULL,
  `match_id` int NOT NULL,
  PRIMARY KEY (`match_id`),
  CONSTRAINT `fk_Shoots_Matches1` FOREIGN KEY (`match_id`) REFERENCES `matches` (`match_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

-- Final view structure for view `avg_odds_bet_sites`
--

/*!50001 DROP VIEW IF EXISTS `avg_odds_bet_sites`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `avg_odds_bet_sites` AS select `b`.`bet_sites_name` AS `bet_sites_name`,avg(((`bso`.`home_odds` + `bso`.`away_odds`) + `bso`.`draw_odds`)) AS `avg_home_odds` from ((`matches` `m` join `bet_sites_odds` `bso` on((`bso`.`match_id` = `m`.`match_id`))) join `bet_sites` `b` on((`b`.`id` = `bso`.`bet_sites_id`))) group by `b`.`bet_sites_name` order by avg(((`bso`.`home_odds` + `bso`.`away_odds`) + `bso`.`draw_odds`)) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `managed_referee`
--

/*!50001 DROP VIEW IF EXISTS `managed_referee`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `managed_referee` AS select `r`.`referee_name` AS `referee_name`,count(`r`.`referee_id`) AS `count(r.referee_id)` from (`referees` `r` join `matches_referees` `mr` on((`r`.`referee_id` = `mr`.`referee_id`))) group by `r`.`referee_name` order by count(`r`.`referee_id`) desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

USE `EPL_DATA`;
DROP procedure IF EXISTS `Red_cards_by_Referee`;

DELIMITER $$

-- Create Stored Procedure Red_cards_by_Referee
USE `EPL_DATA`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Red_cards_by_Referee`(IN ref_name VARCHAR(45), OUT red_cnt INT)
BEGIN
 	select sum(home_red+away_red) into red_cnt
from matches m  join referee_act ra on ra.match_id = m.match_id
 	join referees r on r.referee_id = ra.referee_id
 	where referee_name = ref_name;
END$$

DELIMITER ;


USE `EPL_DATA`;
DROP procedure IF EXISTS `Avg_home_odds_by_bet_site`;

DELIMITER $$

-- Create Stored Procedure Avg_home_odds_by_bet_site
USE `EPL_DATA`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Avg_home_odds_by_bet_site`(INOUT bet_name VARCHAR(45))
BEGIN
select avg(home_odds) INTO bet_name
from bet_sites bs join bet_sites_odds bso on bs.id=bso.bet_sites_id
where bet_sites_id in (select id from bet_sites where bet_sites_name = bet_name) ;
END$$

DELIMITER ;
-- Create Stored Procedure result_of_matches
USE `EPL_DATA`;
DROP procedure IF EXISTS `result_of_matches`;

DELIMITER $$
USE `EPL_DATA`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `result_of_matches`(IN r varchar(1))
 BEGIN
   	 select home_team, away_team,ft_home_goal,ft_away_goal
   	 from Goals_results join matches on matches.match_id = Goals_results.match_id
   	 where ft_result = r;
END$$

DELIMITER ;


